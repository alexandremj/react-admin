import React from 'react';
import { Title } from 'react-admin';
import { Card, CardHeader, CardContent } from '@material-ui/core';

export default () => (
    <Card>
        <Card>
            <Title title="Challenge Kartado - React Admin" />
            <CardHeader title="Challenge Kartado - React Admin"></CardHeader>
        </Card>

        <Card>
            <CardContent>Aplicação desenvolvida: controle de postagens de fotos em um site</CardContent>
            <CardContent>Utilizei para isso três bancos de dados da API: <b>photos</b>, <b>albums</b> e <b>users</b></CardContent>
            <CardContent>Cada foto pertence a um álbum, que é criado por um usuário. </CardContent>
            <CardContent>Ao longo do desenvolvimento, as decisões foram tomadas considerando um usuário final que tenha pouco ou nenhum conhecimento sobre desenvolvimento web, e assim diversas informações importantes de "funcionamento interno" do banco de dados foram suprimidas em troca de uma interface mais agradável ao usuário final.</CardContent>
            <CardContent>Alexandre Müller Júnior, Maio/2020</CardContent>
        </Card>
    </Card>
);