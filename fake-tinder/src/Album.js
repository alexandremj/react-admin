import React from 'react';
import { List, Datagrid, ReferenceField, TextField, Edit, SimpleForm,
        ReferenceInput, SelectInput, TextInput, Create, Filter, SimpleList
        } from 'react-admin';
import { useMediaQuery } from '@material-ui/core';

export const AlbumFilter = props => (
    <Filter {...props}>
        <ReferenceInput label="Fótografo" source="userId" reference="users" allowEmpty>
            <SelectInput optionText="name"/>
        </ReferenceInput>
    </Filter>
);

// couldnt understand how to get the photographer's name on the secondaryText prop
export const AlbumList = props => {
    const smallScreen = useMediaQuery(theme => theme.breakpoints.down('sm'));
    return (
        <List filters={<AlbumFilter/>}{...props}>
        { smallScreen ? (
                <SimpleList
                    primaryText={record=>record.title}
                    secondaryText={record => `Fotógrafo: ${record.userId}`}
                />
            ) : (
                <Datagrid rowClick="edit">
                    <TextField source="title" label="Título do álbum"/>
                    <ReferenceField source="userId" reference="users" label="Fotógrafo">
                        <TextField source="name"/>
                    </ReferenceField>
                </Datagrid>
            )
        }
        </List>
    )
    
};

export const AlbumEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users" label="Fotógrafo">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="title" label="Título do álbum"/>
        </SimpleForm>
    </Edit>
);

export const AlbumCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <ReferenceInput source="userId" reference="users" label="Fotógrafo">
                <SelectInput optionText="name" />
            </ReferenceInput>
            <TextInput source="title" label="Título do álbum"/>
        </SimpleForm>
    </Create>
);