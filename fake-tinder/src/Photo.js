import React from 'react';
import { List, Datagrid, ReferenceField, TextField, ImageField, Edit,
         SimpleForm, ReferenceInput, TextInput, SelectInput, Create,
         ImageInput, Filter, SimpleList
        } from 'react-admin';
import { useMediaQuery } from '@material-ui/core';

// currently displays both Filters at the same time for no particular reason
export const PhotoFilter = props => (
    <Filter {...props}>
        <ReferenceInput label="Álbum" source="albumId" reference="albums" allowEmpty>
            <SelectInput optionText="title"/>
        </ReferenceInput>
        <TextInput label="Título da Foto" source="title" allowEmpty/>
    </Filter>
)

// couldnt understand how to get the album's name on the secondaryText prop
// also the image doesnt display properly on SimpleLists
export const PhotoList = props => {
    const smallScreen = useMediaQuery(theme => theme.breakpoints.down('sm'));

    return (
        <List filters={<PhotoFilter/>} {...props}>
        { smallScreen ? (
            <SimpleList leftAvatar={record => record.url}
                        primaryText={record => record.albumId}/>
        ) : (
            <Datagrid rowClick="edit">
                <ImageField source="url" label="Foto" />
                <ReferenceField source="albumId" reference="albums" label="Álbum">
                    <TextField source="title" />
                </ReferenceField>
                <TextField source="title" label="Título da foto" />
            </Datagrid>
            )
        }
            </List>
    );
};

// doesnt really work; the image is kept as an url on the db so we would need
// a cloud server to store the image. Looks really good on the front-end though
export const PhotoEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <ImageInput source="url" label="" accept="image/*" placeholder={<p>Insira sua imagem aqui</p>}>
                <ImageField source="url" title="title" />
            </ImageInput>

            <ReferenceInput source="albumId" reference="albums" label="Álbum">
                <SelectInput optionText="title" />
            </ReferenceInput>
            <TextInput source="title" label="Título" helperText="Dê um nome para sua foto!" />
        </SimpleForm>
    </Edit>
);

// doesnt really work; the image is kept as an url on the db so we would need
// a cloud server to store the image. Looks really good on the front-end though
export const PhotoCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <ImageInput source="url" label="" accept="image/*" placeholder={<p>Insira sua imagem aqui</p>}>
                <ImageField source="url" title="title" />
            </ImageInput>

            <ReferenceInput source="albumId" reference="albums" label="Álbum">
                <SelectInput optionText="title" />
            </ReferenceInput>
            <TextInput source="title" label="Título" helperText="Dê um nome para sua foto!" />
        </SimpleForm>
    </Create>
);