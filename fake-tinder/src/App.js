import React from 'react';
import { Admin, Resource } from 'react-admin';
import jsonServerProvider from 'ra-data-json-server';

// material icons import
import CameraAltIcon from '@material-ui/icons/CameraAlt';
import DesktopWindowsIcon from '@material-ui/icons/DesktopWindows'; // meant for use on the dashboard
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import EmojiPeopleIcon from '@material-ui/icons/EmojiPeople';

// Components
import { AlbumList, AlbumEdit, AlbumCreate }  from './Album';
import { UserList, UserEdit, UserCreate } from './User';
import { PhotoList, PhotoEdit, PhotoCreate } from './Photo';

import Dashboard from './Dashboard';
import authProvider from './authProvider';

const dataProvider = jsonServerProvider('https://jsonplaceholder.typicode.com');

const App = () => (
    <Admin dashboard={Dashboard} authProvider={authProvider} dataProvider={dataProvider} icon={DesktopWindowsIcon}>
        <Resource name="users" list={UserList} edit={UserEdit} create={UserCreate} icon={EmojiPeopleIcon} options={{ label: "Usuários" }}/>
        <Resource name="photos" list={PhotoList} edit={PhotoEdit} create={PhotoCreate} icon={CameraAltIcon} options={{ label: "Fotos" }}/>
        <Resource name="albums" list={AlbumList} edit={AlbumEdit} create={AlbumCreate} icon={PhotoLibraryIcon} options={{ label: "Álbuns" }} />
    </Admin>
);

export default App;
