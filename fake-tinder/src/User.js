import React from 'react';
import { Filter, List, Datagrid, TextField, EmailField, Edit, SimpleForm, TextInput,
        ReferenceInput, SelectInput, Create, SimpleList } from 'react-admin';
import { useMediaQuery } from '@material-ui/core';

export const UserFilter = props => (
    <Filter {...props}>
        <ReferenceInput label="Fótografo" source="userId" reference="users" allowEmpty>
            <SelectInput optionText="name" />
        </ReferenceInput>
        <ReferenceInput label="Nome de usuário" source="username" reference="users" allowEmpty>
            <SelectInput optionText="username" />
        </ReferenceInput>
        <ReferenceInput label="E-mail" source="email" reference="users" allowEmpty>
            <SelectInput optionText="email" />
        </ReferenceInput>
    </Filter>
);

export const UserList = props => {
    const smallScreen = useMediaQuery(theme => theme.breakpoints.down('sm'));

    return (
        <List filters={<UserFilter/>} {...props}>
        { smallScreen?
            (
                <SimpleList primaryText={record => `${record.name} - @${record.username}`}
                            secondaryText={record => `${record.phone}`} />
            ) : (
                <Datagrid rowClick="edit">
                    <TextField source="name" label="Fotógrafo"/>
                    <TextField source="username" label="Nome de usuário"/>
                    <EmailField source="email" label="E-mail"/>
                    <TextField source="phone" label="WhatsApp"/>
                </Datagrid>
            )
        }
        </List>
    );
    
};

export const UserEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="name" label="Fotógrafo"/>
            <TextInput source="username" label="Nome de usuário"/>
            <TextInput source="email" label="E-mail"/>
            <TextInput source="phone" label="WhatsApp"/>
        </SimpleForm>
    </Edit>
);

export const UserCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" label="Fotógrafo"/>
            <TextInput source="username" label="Nome de usuário"/>
            <TextInput source="email" label="E-mail"/>
            <TextInput source="phone" label="WhatsApp"/>
        </SimpleForm>
    </Create>
);